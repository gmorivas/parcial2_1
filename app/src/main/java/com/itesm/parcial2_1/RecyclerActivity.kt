package com.itesm.parcial2_1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class RecyclerActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var recyclerView: RecyclerView
    lateinit var data: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler)

        // recycler view - widget used to display a collection of objects
        recyclerView = findViewById(R.id.recyclerView)

        // data (100% abstract) - Adapter (helps translate abstract data into a view) -  recycler view (GUI, very concrete)
        data = ArrayList()
        data.add("fido")
        data.add("firulais")
        data.add("solovino")
        data.add("fifi")
        data.add("killer")
        data.add("fido")
        data.add("firulais")
        data.add("solovino")
        data.add("fifi")
        data.add("killer")
        data.add("fido")
        data.add("firulais")
        data.add("solovino")
        data.add("fifi")
        data.add("killer")
        data.add("fido")
        data.add("firulais")
        data.add("solovino")
        data.add("fifi")
        data.add("killer")
        data.add("fido")
        data.add("firulais")
        data.add("solovino")
        data.add("fifi")
        data.add("killer")

        // declare  the adapter
        val adapter = DogAdapter(data, this)

        // we need to declare a layout manager
        // layout manager is the one in charge of arranging the "rows" within the recycler view
        var llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        var glm = GridLayoutManager(this, 2)

        // setup the recycler view
        recyclerView.layoutManager = llm
        recyclerView.adapter = adapter

    }

    override fun onClick(row: View) {
        val position = recyclerView.getChildLayoutPosition(row)
        Toast.makeText(this, data[position], Toast.LENGTH_SHORT).show();
    }
}