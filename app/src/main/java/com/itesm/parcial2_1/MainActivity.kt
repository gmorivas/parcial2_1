package com.itesm.parcial2_1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment

class MainActivity : AppCompatActivity(), DogFragment.Callback {

    lateinit var catFragment: CatFragment
    lateinit var dogFragment: DogFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // como crear objetos de tipo fragment
        catFragment = CatFragment()
        dogFragment = DogFragment.newInstance("firulais", 5, 20.2f)

        // como manipular fragmentos desde código
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragmentContainerView, dogFragment, TAG_FRAGMENTO)
        transaction.commit()
    }

    fun intercambiarFragmentos(v: View?){

        // obtener el fragmento actual para ver cual pongo

        val fragmentoActual = supportFragmentManager.findFragmentByTag(TAG_FRAGMENTO)

        if(fragmentoActual != null){

            // lo hacemos dog por default
            var fragmentoNuevo : Fragment = dogFragment

            // verificamos si es dog o no
            if(fragmentoActual == dogFragment)
                fragmentoNuevo = catFragment

            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragmentContainerView, fragmentoNuevo, TAG_FRAGMENTO)
            transaction.commit()

        }
    }

    fun doSomethingOnFragment(view : View?){

        dogFragment.doLogicOnFragment()
    }

    fun goToRecyclerActivity(view: View?){

        val intent = Intent(this, RecyclerActivity::class.java)
        startActivity(intent)
    }

    fun goToMapActivity(view: View?){

        val intent = Intent(this, MapsActivity::class.java)
        startActivity(intent)
    }

    companion object {

        private const val TAG_FRAGMENTO = "fragmentito"
    }

    override fun doSomething() {
        Toast.makeText(this, "THIS WAS INVOKED FROM A FRAGMENT!", Toast.LENGTH_SHORT).show()
    }
}