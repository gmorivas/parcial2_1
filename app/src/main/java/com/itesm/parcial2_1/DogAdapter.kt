package com.itesm.parcial2_1

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

// super important for this class
// have a data source!
class DogAdapter(private var doggies : ArrayList<String>, private var listener : View.OnClickListener) : RecyclerView.Adapter<DogAdapter.DogViewHolder>() {

    // start with the view holder
    // view holder is a class from which we will create an object that will be used to manage a view
    class DogViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var text : TextView
        var button : Button

        // main constructor has no body
        // BUT there is an init block that is always called
        init {

            text = itemView.findViewById(R.id.rowTextView)
            button = itemView.findViewById(R.id.rowButton)
        }
    }

    // moment in which we create the view for a new row
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DogViewHolder {

        // 1st we need to get a view
        // to do so we are going to use the inflater!
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row, parent, false)
        val dvh = DogViewHolder(view)

        dvh.button.setOnClickListener{

            Toast.makeText(parent.context, "SOME ACTION HAPPENED!", Toast.LENGTH_SHORT).show()
        }

        view.setOnClickListener(listener)
        return dvh
    }

    // bind?
    // associate, attach, relate
    // invoked when the adapter wants to set a specific item's info to a view holder
    override fun onBindViewHolder(holder: DogViewHolder, position: Int) {

        holder.text.text = doggies[position]
        holder.button.text = doggies[position]
    }

    // get the total amount of elements that we intend to display
    override fun getItemCount(): Int {

        return doggies.size
    }
}