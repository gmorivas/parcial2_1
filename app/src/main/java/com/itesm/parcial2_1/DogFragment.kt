package com.itesm.parcial2_1

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView


private const val ARG_NAME = "name"
private const val ARG_AGE = "age"
private const val ARG_WEIGHT = "weight"

class DogFragment : Fragment() {
    private var name: String? = null
    private var age: Int = 0
    private var weight: Float = 0f
    private var listener : Callback? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            name = it.getString(ARG_NAME)
            age = it.getInt(ARG_AGE)
            weight = it.getFloat(ARG_WEIGHT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_dog, container, false)

        val nameUI = view.findViewById<TextView>(R.id.dog_name)
        val ageUI = view.findViewById<TextView>(R.id.dog_age)
        val weightUI = view.findViewById<TextView>(R.id.dog_weight)
        val button1 = view.findViewById<Button>(R.id.button)
        val button2 = view.findViewById<Button>(R.id.button2)

        nameUI.text = name
        ageUI.text = age.toString()
        weightUI.text = weight.toString()

        button1.setOnClickListener {

            Log.wtf("FRAGMENT", "BUTTON 1 WAS PRESSED")
        }

        button2.setOnClickListener {

            listener?.doSomething()
        }

        return view
    }

    public fun doLogicOnFragment(){

        Log.wtf("FRAGMENT", "THIS HAPPENED! ")
    }

    interface Callback {

        fun doSomething()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        listener = if(context is Callback) {
            context
        } else {
            throw RuntimeException("ACTIVITY MUST IMPLEMENT CALLBACK INTERFACE TO WORK")
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(name: String, age: Int, weight: Float) =
            DogFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_NAME, name)
                    putInt(ARG_AGE, age)
                    putFloat(ARG_WEIGHT, weight)
                }
            }
    }


}